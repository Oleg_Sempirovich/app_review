package com.example.myapplication.di.module.datasource

import com.example.myapplication.data.dao.AppDB
import com.example.myapplication.data.datasource.UserCachedDataSourceImpl
import com.example.myapplication.data.datasource.UserRemoteDataSourceImpl
import com.example.myapplication.data.network.UserApi
import com.example.myapplication.di.provider.RetrofitProvider
import com.example.myapplication.domain.datasource.UserCachedDataSource
import com.example.myapplication.domain.datasource.UserRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class UserDataSourceModule {

    @Binds
    abstract fun bindUserRemoteDataSource(
        remote: UserRemoteDataSourceImpl
    ): UserRemoteDataSource

    @Binds
    abstract fun bindUserCachedDataSource(
        cached: UserCachedDataSourceImpl
    ): UserCachedDataSource

    companion object {
        @Provides
        fun provideUserApi(retrofitServiceProvider: RetrofitProvider) =
            retrofitServiceProvider.createService(UserApi::class.java)

        @Provides
        fun provideUserDb(appDB: AppDB) =
            appDB.userDao()
    }

}
