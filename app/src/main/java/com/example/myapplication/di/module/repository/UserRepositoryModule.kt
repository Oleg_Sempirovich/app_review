package com.example.myapplication.di.module.repository

import com.example.myapplication.data.repository.UserRepositoryImpl
import com.example.myapplication.di.module.datasource.UserDataSourceModule
import com.example.myapplication.di.provider.RetrofitProvider
import com.example.myapplication.domain.repository.user.UserRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [UserDataSourceModule::class])
abstract class UserRepositoryModule {

    @Binds
    abstract fun bindUserRepository(
        userRepository: UserRepositoryImpl
    ): UserRepository

}

