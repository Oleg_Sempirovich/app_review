package com.example.myapplication.di.module.activity

import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.di.module.fragment.UserListFragmentModule
import com.example.myapplication.di.provider.factory.ViewModelFactory
import com.example.myapplication.di.scope.PerFragment
import com.example.myapplication.presentation.fragment.UserListFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @Binds
    internal abstract fun bindViewModelFactory(
        factory: ViewModelFactory
    ): ViewModelProvider.Factory

    @PerFragment
    @ContributesAndroidInjector(modules = [UserListFragmentModule::class])
    internal abstract fun provideUserListFragment(): UserListFragment


}
