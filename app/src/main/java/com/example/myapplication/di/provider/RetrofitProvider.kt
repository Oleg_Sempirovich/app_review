package com.example.myapplication.di.provider

import android.content.Context
import com.example.myapplication.BuildConfig
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitProvider(context: Context){

        private val retrofit: Retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .client(OkHttpClient.Builder().apply {
                    addInterceptor(ChuckInterceptor(context))
                }.build())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        }

        fun <S> createService(serviceClass: Class<S>): S = retrofit.create(serviceClass)

    }