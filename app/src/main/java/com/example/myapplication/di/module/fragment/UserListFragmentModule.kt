package com.example.myapplication.di.module.fragment

import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import com.example.myapplication.R
import com.example.myapplication.di.module.repository.UserRepositoryModule
import com.example.myapplication.di.scope.viewmodel.ViewModelKey
import com.example.myapplication.domain.DataUser
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter
import com.example.myapplication.presentation.viewmodel.UserListViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module(includes = [UserRepositoryModule::class])
abstract class UserListFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    abstract fun bindViewModel(viewModel: UserListViewModel): ViewModel

    companion object {

        @Provides
        fun provideAdapterDataUser(viewViewModel: UserListViewModel): BaseDataBindAdapter<DataUser> =
            BaseDataBindAdapter(
                R.layout.item_user,
                BR.dataUser,
                viewModel = viewViewModel
            )

    }
}