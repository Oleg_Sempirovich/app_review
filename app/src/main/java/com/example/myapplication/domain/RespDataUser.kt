package com.example.myapplication.domain

import com.squareup.moshi.Json

data class RespDataUser(@field:Json(name = "data")val userListData:List<DataUser>)