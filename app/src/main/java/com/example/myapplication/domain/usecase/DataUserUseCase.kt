package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.repository.user.UserRepository
import javax.inject.Inject

class DataUserUseCase @Inject constructor(
    private val repository: UserRepository
) {
    suspend fun execute() = repository.getUserList()
}
