package com.example.myapplication.domain.datasource

import android.service.autofill.UserData
import com.example.myapplication.domain.RespDataUser
import com.example.myapplication.domain.entity.DataUserEntity
import com.example.myapplication.domain.handleerror.ResultWrapper

interface UserRemoteDataSource {

    suspend fun getUserList(): ResultWrapper<RespDataUser>
}

interface UserCachedDataSource {

    suspend fun insertUserList(
        userList: List<DataUserEntity>
    )
    suspend fun getDataUserEntityList():
       List<DataUserEntity>

}
