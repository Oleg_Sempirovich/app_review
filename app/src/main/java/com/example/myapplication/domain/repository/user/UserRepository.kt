package com.example.myapplication.domain.repository.user

import com.example.myapplication.domain.RespDataUser
import com.example.myapplication.domain.entity.DataUserEntity
import com.example.myapplication.domain.handleerror.ResultWrapper

interface UserRepository {

    suspend fun getUserList(): ResultWrapper<RespDataUser>

    suspend fun getUserListEntity(): List<DataUserEntity>

}
