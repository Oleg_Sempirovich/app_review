package com.example.myapplication

import com.example.myapplication.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out App?> = DaggerAppComponent.factory()
        .create(this)
}
