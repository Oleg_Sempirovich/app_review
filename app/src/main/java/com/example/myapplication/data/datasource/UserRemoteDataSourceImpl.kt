package com.example.myapplication.data.datasource

import com.example.myapplication.data.network.UserApi
import com.example.myapplication.domain.RespDataUser
import com.example.myapplication.domain.datasource.UserRemoteDataSource
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.handleerror.safeApiCall
import javax.inject.Inject

class UserRemoteDataSourceImpl @Inject constructor(
    private val api: UserApi,
) : UserRemoteDataSource {

    override suspend fun getUserList(): ResultWrapper<RespDataUser> {
        return safeApiCall { api.getUserListData() }
    }
}
