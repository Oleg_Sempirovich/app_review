package com.example.myapplication.data.datasource

import com.example.myapplication.data.dao.usedata.DataUserDao
import com.example.myapplication.domain.datasource.UserCachedDataSource
import com.example.myapplication.domain.entity.DataUserEntity
import javax.inject.Inject

class UserCachedDataSourceImpl @Inject constructor(
    private val userDao: DataUserDao
) : UserCachedDataSource {

    override suspend fun insertUserList(userList: List<DataUserEntity>) {
        userDao.insertAll(userList)
    }

    override suspend fun getDataUserEntityList() =
        userDao.getUserEntityAll()
}
