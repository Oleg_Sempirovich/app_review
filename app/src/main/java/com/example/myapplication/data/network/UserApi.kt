package com.example.myapplication.data.network

import com.example.myapplication.domain.RespDataUser
import retrofit2.http.GET

interface UserApi {

    @GET("users")
    suspend fun getUserListData(): RespDataUser
}