package com.example.myapplication.data.repository

import com.example.myapplication.data.datasource.UserCachedDataSourceImpl
import com.example.myapplication.data.datasource.UserRemoteDataSourceImpl
import com.example.myapplication.domain.repository.user.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val remote: UserRemoteDataSourceImpl,
    private val cache: UserCachedDataSourceImpl,
) : UserRepository {

    override suspend fun getUserList() =
        remote.getUserList()

    override suspend fun getUserListEntity() =
       cache.getDataUserEntityList()

}
