package com.example.myapplication.data.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.data.dao.usedata.DataUserDao
import com.example.myapplication.domain.entity.DataUserEntity

@Database(
    entities = [
        DataUserEntity::class],
    version = 1
)

abstract class AppDB : RoomDatabase() {

    companion object {
        const val NAME = "app_database"
    }

    abstract fun userDao(): DataUserDao

}
