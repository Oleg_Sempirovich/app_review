package com.example.myapplication.data.dao.usedata

import androidx.room.*
import com.example.myapplication.domain.entity.DataUserEntity

@Dao
interface DataUserDao{

    @Query("SELECT * FROM data_user")
    suspend fun getUserEntityAll(): List<DataUserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(entities: List<DataUserEntity>)

    @Delete
    fun delete(user: DataUserEntity)
}

