package com.example.myapplication.extension

import androidx.annotation.ColorInt
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.domain.DataUser
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter

fun Toolbar.setState(
    textData: String, @ColorInt colorBackground: Int
) {
    subtitle = null
    title = textData
    setBackgroundColor(colorBackground)
}

fun RecyclerView.setAdapterBindOrder(
    dataUser: List<DataUser>?,
    adapterOrder: BaseDataBindAdapter<DataUser>?
) {
    if (adapter == null) {
        layoutManager = LinearLayoutManager(context)
        adapter = adapterOrder
    }
    dataUser?.also {
        adapterOrder?.dataList = it
    }

}