package com.example.myapplication.presentation.fragment

import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentUserListBinding
import com.example.myapplication.domain.DataUser
import com.example.myapplication.extension.setAdapterBindOrder
import com.example.myapplication.extension.setState
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter
import com.example.myapplication.presentation.fragment.base.BaseFragment
import com.example.myapplication.presentation.viewmodel.UserListViewModel
import javax.inject.Inject


class UserListFragment: BaseFragment<FragmentUserListBinding>(R.layout.fragment_user_list) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit  var userAdapter: BaseDataBindAdapter<DataUser>

    private val vm by viewModels<UserListViewModel> { viewModelFactory }


    override fun initBinding() {
        initToolBar()
        initRecyclerView()
    }

    private fun initToolBar() {
       binding.toolbar.setState(getString(R.string.app_name),ContextCompat.getColor(requireContext(),R.color.colorPrimary))
    }

    private fun initRecyclerView() {
        binding.userListRecyclerView.setAdapterBindOrder(arrayListOf(),userAdapter)
    }

    override fun observeData() {
       vm.navigationLiveData.observe(this){
           userAdapter.dataList = it
       }
    }

}