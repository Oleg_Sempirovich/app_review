package com.example.myapplication.presentation.viewmodel

import android.service.autofill.UserData
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.example.myapplication.domain.DataUser
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.usecase.DataUserUseCase
import com.example.myapplication.presentation.base.SingleLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject


class UserListViewModel  @Inject constructor(val userUseCase: DataUserUseCase) : ViewModel() {

    private val _navigationLiveData = SingleLiveEvent<List<DataUser>>()
    val navigationLiveData: LiveData<List<DataUser>> = _navigationLiveData


    init {
       initUserData()
    }

    private fun initUserData(){
        viewModelScope.launch {

            when (val result = userUseCase.execute()) {
                is ResultWrapper.Success -> {
                    result.value.userListData.also {
                        _navigationLiveData.value = it
                    }

                }
                is ResultWrapper.Error.HttpError -> {
                    /**
                     * logic http error data
                     */
                }
                else -> {
                    /**
                     * logic general error data
                     */
           /**/     }
            }

        }
    }

    fun clickOrder(dataUser: DataUser){
        /**
         * logic click
         */
    }
}
